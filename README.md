# App Library (Experimental)
[ ![Download](https://api.bintray.com/packages/seventhunders07/app-template-library/app-template-library/images/download.svg) ](https://bintray.com/seventhunders07/app-template-library/app-template-library/_latestVersion)
</br>


This library is used alongside with the [APP Template](https://gitlab.com/seventhunders07/app-template) project for easy implementation of the template.<br>
Uses the Android Architecture that was presented during Google IO17.

## How To Use
#### Project Level (build.gradle)
``` 
allprojects {
    repositories {
        ...
        maven { url "https://dl.bintray.com/seventhunders07/app-template-library"}
    }
}
```

#### App Level (build.gradle)
```
dependencies{
    ...
    compile 'com.template.app:applibrary:{Latest Version}'
}
```

### Please feel free to contribute on this repo.
