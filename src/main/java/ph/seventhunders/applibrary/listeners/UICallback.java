package ph.seventhunders.applibrary.listeners;

/**
 * Created by root on 2/28/18.
 */

public interface UICallback {

    void setText(String text);

    void showInputDialog(String message, String title);
}
