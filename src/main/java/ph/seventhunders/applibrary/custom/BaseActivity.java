package ph.seventhunders.applibrary.custom;

import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import de.mateware.snacky.Snacky;
import ph.seventhunders.applibrary.R;

/**
 * Created by root on 2/28/18.
 */

public abstract class BaseActivity extends AppCompatActivity implements LifecycleOwner{

    public Context mContext;
    private AlertDialog alertDialog;
    private ArrayList<Boolean> permissionResults;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayoutResource());
        ButterKnife.bind(this);
        mContext = this;
    }

    public abstract int setLayoutResource();

    public void setToolbar(Toolbar toolbar){
       setSupportActionBar(toolbar);
    }

    public void setToolbarTitle(String title, boolean canGoBack){
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(canGoBack);
    }

    public void showSnackbarSuccess(String msg, String actionTitle, @Nullable View.OnClickListener onClickListener) {
        Snacky.builder().setActivity(this).setTextColor(Color.WHITE).success().setText(msg).setAction(actionTitle, onClickListener)
                .setActionTextColor(getResources().getColor(R.color.colorWhite)).show();
    }

    public void showSnackbarError(String message, String actionTitle, @Nullable View.OnClickListener onClickListener) {
        Snacky.builder().setActivity(this).setText(message).error().setAction(actionTitle, onClickListener)
                .setActionTextColor(getResources().getColor(R.color.colorWhite)).show();
    }

    public void showSnackbarWarning(String message, String actionTitle, @Nullable View.OnClickListener onClickListener) {
        Snacky.builder().setActivity(this).setText(message).warning().setAction(actionTitle, onClickListener)
                .setActionTextColor(getResources().getColor(R.color.colorWhite)).show();
    }

    public void showSnackbarInfo(String message, String actionTitle, @Nullable View.OnClickListener onClickListener) {
        Snacky.builder().setActivity(this).setText(message).info().setAction(actionTitle, onClickListener)
                .setActionTextColor(getResources().getColor(R.color.colorWhite)).show();
    }

    protected void checkForPermissions(int requestCode, String... permissions) {
        List<String> permissionsNeeded = new ArrayList<>();

        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsNeeded.add(permission);
            }
        }

        if (permissionsNeeded.size() > 0) {
            ActivityCompat.requestPermissions(this, permissionsNeeded.toArray(new String[permissionsNeeded.size()]), requestCode);
            return;
        }
        Log.d("BaseActivity", "checkForPermissions: granted");
        permissionGranted(requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 1) {
            // Handle multiple permission request
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    permissionDenied("Some Permission is Denied", requestCode, permissions);
                    return;
                }
            }
            permissionGranted(requestCode);
        } else {
            // Handle single permission request
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionGranted(requestCode);
            } else {
                permissionDenied("Permission is Denied", requestCode, permissions);
            }
        }
    }

    //shows rationale dialog if user denied permission
    private void showRationale(int permission_call, final int requestCode, final String... permission) {
        new MaterialDialog.Builder(this)
                .content(getResources().getString(permission_call))
                .title("Permission Denied")
                .positiveText("Retry")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        checkForPermissions(requestCode, permission);
                    }
                })
                .negativeText("I'm sure")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        rationaleDialogDenial(dialog);
                    }
                })
                .canceledOnTouchOutside(false)
                .autoDismiss(true)
                .show();
    }

    protected void permissionGranted(int requestCode) {
    }

    protected void permissionDenied(String message, int requestCode, String... permissions) {
        resolvePermissionDenial(requestCode, permissions);
        //Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void rationaleDialogDenial(MaterialDialog dialog) {
        onBackPressed();
    }

    public void removeToolbarTitle() {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void showRationaleInstructions() {
        new MaterialDialog.Builder(this)
                .title("Feature Denied")
                .content(R.string.permission_denied)
                .canceledOnTouchOutside(false)
                .positiveText("OK, I get it.")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        rationaleDialogDenial(dialog);
                    }
                })
                .show();
    }

    private boolean checkIfShouldShowRationale(String[] permissions) {
        permissionResults = new ArrayList<>();
        for (String permission : permissions) {
            permissionResults.add(ActivityCompat.shouldShowRequestPermissionRationale(this, permission));
        }
        return areAllTrue(permissionResults);
    }

    private boolean areAllTrue(ArrayList<Boolean> booleanArrayList) {
        for (Boolean b : booleanArrayList)
            if (!b) return false;
        return true;
    }

    private int countAllTrue(ArrayList<Boolean> booleanArrayList) {
        int index = 0;
        for (Boolean b : booleanArrayList) {
            if (b) {
                index++;
                continue;
            }
        }
        return index;
    }

    private void resolvePermissionDenial(int requestCode, String[] permissions) {
        // permission denied, boo! Disable the
        // functionality that depends on this permission.
        boolean showRationale = false;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            showRationale = checkIfShouldShowRationale(permissions);
        }
        if (!showRationale) {
            // user also CHECKED "never ask again"
            // you can either enable some fall back,
            // disable features of your app
            // or open another dialog explaining
            // again the permission and directing to
            // the app setting
            if (countAllTrue(permissionResults) == 0) {
                showRationaleInstructions();
                return;
            }
            showRationale(R.string.permission_denied_temporary, requestCode, permissions);
        } else {
            showRationale(R.string.permission_denied_temporary, requestCode, permissions);
            // user did NOT check "never ask again"
            // this is a good place to explain the user
            // why you need the permission and ask if he wants
            // to accept it (the rationale)
        }
    }


}
