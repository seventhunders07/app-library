package ph.seventhunders.applibrary.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import ph.seventhunders.applibrary.custom.BaseFragment;

/**
 * Created by root on 3/5/18.
 */

public class DynamicFragmentPagerAdapter extends FragmentPagerAdapter {

    ArrayList<BaseFragment> mPagerScreens = new ArrayList<>();
    ArrayList<String> mPageTitle = new ArrayList<>();
    public DynamicFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mPagerScreens.get(position);
    }

    @Override
    public int getCount() {
        return mPagerScreens.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitle.get(position);
    }

    /**
     * adds screen to view pager
     * @param baseFragment screen to add to view pager
     * @param title title for the screen (e.g tab pager)
     */
    public void addScreen(BaseFragment baseFragment, @Nullable String title){
        mPagerScreens.add(baseFragment);
        mPageTitle.add(title);
    }
}
