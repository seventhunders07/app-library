package ph.seventhunders.applibrary.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {


    private static Bitmap bitmap;

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static int getIntegerFromFloatDp(Context context, float value) {
        return Math.round(pxFromDp(context, value));
    }

    public static double removeInsignificantDigits(double distance) {
        return (double) Math.round(distance * 100.0) / 100.0;
    }

    public static boolean fieldValidator(AppCompatEditText... appCompatEditTexts) {
        for (AppCompatEditText appCompatEditText : appCompatEditTexts) {
            if (appCompatEditText.getText().toString().isEmpty()) {
                return false;
            }
            if (appCompatEditText.getHint().toString().equalsIgnoreCase("Email Address") && !isEmailValid(appCompatEditText.getText().toString())) {
                return false;
            }

            if (appCompatEditText.getHint().toString().equalsIgnoreCase("Password") && appCompatEditText.getText().toString().length() < 6) {
                return false;
            }

            if (appCompatEditText.getHint().toString().equalsIgnoreCase("Phone Number") && appCompatEditText.getText().toString().length() > 11) {
                return false;
            }
        }
        return true;
    }

    public static String findInvalidField(AppCompatEditText... appCompatEditTexts) {
        for (AppCompatEditText appCompatEditText : appCompatEditTexts) {
            if (appCompatEditText.getText().toString().isEmpty()) {
                appCompatEditText.requestFocus();
                return appCompatEditText.getHint().toString() + " is required to be able to register, Please check your inputs and try again.";
            }
            if (appCompatEditText.getHint().toString().equalsIgnoreCase("Email Address") && !isEmailValid(appCompatEditText.getText().toString())) {
                appCompatEditText.requestFocus();
                return appCompatEditText.getHint().toString() + " is not a valid email. Please check your inputs and try again.";
            }

            if (appCompatEditText.getHint().toString().equalsIgnoreCase("Password") && appCompatEditText.getText().toString().length() < 6) {
                appCompatEditText.requestFocus();
                return appCompatEditText.getHint().toString() + " must be at least 6 characters. Please check your inputs and try again.";
            }
            if (appCompatEditText.getHint().toString().equalsIgnoreCase("Phone Number") && appCompatEditText.getText().toString().length() > 11) {
                appCompatEditText.requestFocus();
                return appCompatEditText.getHint().toString() + " must not be greater than 11(Eleven).";
            }
        }
        return "";
    }

    public static boolean isEmailValid(String email) {
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email);
        return m.find();
    }




    public static Uri resourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }


    public static String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }


}
