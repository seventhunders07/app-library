package ph.seventhunders.applibrary.eventbus;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by root on 3/5/18.
 */

/**
 * Register : GlobalBus.getBus().register(this);
 * Un-Register : GlobalBus.getBus().unregister(this);
 * Broadcast an event : GlobalBus.getBus().post(eventName);
 * Receive an event : you need to write a method with annotation @Subscribe as below.
 *
 * Events - from class event compilation
 * EventName - static classname inside EVENTS
 * event - variable name
 * @Subscribe
 * public void getMessage(Events.EventName event) {
 *
 * }
 */
public class GlobalBus {
    private static EventBus sBus;

    public static EventBus getBus() {
        if (sBus == null)
            sBus = EventBus.getDefault();
        return sBus;
    }
}
